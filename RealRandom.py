import random, time

count = ""
string = ""
specsymvols = ""
symvols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$%&()-+\/"

wait = random.uniform(0.01, 0.25)
time.sleep(int(wait))
def setcount():
    global count
    count = input("\nHow many symvols ➤➤➤  ")
    try:
        int(count)
    except:
        print("\n✘ Int only allowed! (4, 6, 8, 2048)")
        setcount()
setcount()
def setsc():
    global specsymvols
    specsymvols = input("\nUse special symvols [y]es [n]o ➤➤➤  ")
    if specsymvols.lower() != "y" and specsymvols.lower() != "n" and specsymvols.lower() != "":
        print("\n✘ y or n only allowed!")
        setsc()
setsc()
random.seed(random.randint(0, 10000000000000) + random.randint(0, 10000000000000))
while int(len(string)) != int(count) or int(len(string)) < int(count):
    if specsymvols.lower() == "y" or specsymvols.lower() == "":
        symcoun = random.randint(0, len(symvols)-1)
    else:
        symcoun = random.randint(0, len(symvols)-12)
    string = str(string) + str(symvols[symcoun])

print(f"\n{string}\n")
